import matplotlib.pyplot as plt

# --------------------------- plot of the model fitting for the choosen frame(s) --------------------------- # 
def modelFit(frame, numberOfTotalFrame, freqData, ampFFTData, meanAmpFFT, minAmpFFTData, maxAmpFFTData, leftWindow, rightWindow, yGL, yHask, yModel, yModelCube, plotPathName, DoLoop=False, exportThisFit=True, displayPlot=False):
    """ 
    mandatory parameters :
        - frame: the frame in which do the plot if only 1 frame  is displayed
        - numberOfTotalFrame: (array) the number of total frame in case you have multiple frames to plot/save. If one frame used: set it to 1
        - freqData: (array) the frequency of your data
        - ampFFTData : (array) the FFT of amplitude of your data
        - meanAmpFFT : the mean value of th FFT of amplitude of your data
        - minAmpFFTData, maxAmpFFTData : the minimal and maximal values of amplitude of your Data 
        - left/rightWindow : the left and right boundaries of your arrays freqData, ampFFTData and the 4 models. set left to 0 and right to -1 to display all of your data.
            --> usefull if you want to display only a portion of your data to show something or if you realise noise is present on the side(s) of your plot and want to cut it. 
        - yGL, yHask, yModel, yModelCube : (arrays) the function of your models
    
    optional parameters:
        - DoLoop: If DoLoop true, plot every frames from 0 to the last frame. Default to False.
        - exportThisFit: export the fit if set to True. Default to True.
        - displayPlot: display the plot if True. If is set to false (the default value), you will not plot the model on your frame(s):
            --> Usefull when you have a lot of frames and  you want to fit and save all of them.
            --> If you have a lot of frames (e.g. more than 20), displaying all of them may crashes the script because it will use too much RAM.
            --> So you want to save them in a folder, then open it with an image viewer one by one.

    return: the image(s) of the plot(s)  
    """
    if DoLoop == False:
        plotModel = plt.figure(1, figsize=(20,10))
        plt.plot(freqData[frame], ampFFTData[frame], color='k', label='FFT of data', linewidth=0.5) # plot all the data used
        # plot the window used for calculating sigma and standard deviation:
        plt.plot(freqData[frame][leftWindow:rightWindow], ampFFTData[frame][leftWindow:rightWindow], color='lightgray', label='window for calculating sigma', alpha=0.8)

        plt.plot(freqData[frame], yGL[frame], label='linear regression of model (GL)')
        plt.plot(freqData[frame], yHask[frame], color="chartreuse", label='linear regression of model (Haskell)')
        plt.plot(freqData[frame], yModel[frame], color="r", label='linear regression of seismic model')
        plt.plot(freqData[frame], yModelCube[frame], color="r", label='linear regression of seismic model')

        plt.axhline(y=meanAmpFFT[frame], color ="r", linestyle="--", label="mean of amplitude calculated for the window")

        plt.xscale('log')
        plt.yscale('log')
        plt.ylim(minAmpFFTData, maxAmpFFTData)
        plt.title("Fitting of different model with the data of the spectrum of Apollo 15 at frame number %i" %frame)
        plt.xlabel('frequency (Hz)')
        plt.ylabel('amplitude (m/Hz^0.5)')
        plt.legend(loc = 3, prop={'size': 10})                           # legend position
        plt.tight_layout()

        if exportThisFit :
            nameFile = plotPathName
            nameFile = plotPathName+str(frame)+".png"
            plt.savefig(nameFile)   

        """ if you don't want to plot the frame(s), set displayPlot to false hen you call the function """
        if displayPlot == False:
            plt.close(plotModel)     
    else:
        for i in range(numberOfTotalFrame):
            plotModel = plt.figure(i, figsize=(20,10))
            plt.plot(freqData[i], ampFFTData[i], color='k', label='FFT of data', linewidth=0.5) # plot all the data used
            # plot the window used for calculating sigma and standard deviation:
            plt.plot(freqData[i][leftWindow:rightWindow], ampFFTData[i][leftWindow:rightWindow], color='lightgray', label='window for calculating sigma', alpha=0.8)

            plt.plot(freqData[i], yGL[i],                            label='linear regression of model (GL)')
            plt.plot(freqData[i], yHask[i],      color="chartreuse", label='linear regression of model (Haskell)')
            plt.plot(freqData[i], yModel[i],     color="r",          label='linear regression of seismic model')
            plt.plot(freqData[i], yModelCube[i], color="orange",     label='linear regression of cube seismic model')

            plt.axhline(y=meanAmpFFT[i], color ="r", linestyle="--", label="mean of amplitude calculated for the window")

            plt.xscale('log')
            plt.yscale('log')
            plt.ylim(minAmpFFTData, maxAmpFFTData)
            plt.title("Fitting of different model with the data of the spectrum of Apollo 15 at frame number %i" %i)
            plt.xlabel('frequency (Hz)')
            plt.ylabel('amplitude (m/Hz^0.5)')
            plt.legend(loc = 3, prop={'size': 10})                           # legend position
            plt.tight_layout()

            if exportThisFit :
                nameFile = plotPathName+str(i)+".png"
                plt.savefig(nameFile)
            
            """ if you don't want to plot the frame(s), set displayPlot to false hen you call the function """
            if displayPlot == False:
                plt.close(plotModel)

    """ return the figure (can be multiple figures) so you can display it/them if displayPlot == True """
    return plotModel

# --------------------------------------- plot of amplitude and omega_c calculated in the fit  -------------------------------------- # 
def plotOmegaC(time, bGL, cHask, bModel, bModelCube, displayPlot=True):
    plotOmegaC = plt.figure(1503, figsize=(20,10))
    plt.title("Plot of omega_coins/time")
    plt.plot(time, bGL, "b",             label='GL (corrected)',        linewidth=1)
    plt.plot(time, cHask, "chartreuse",  label='Haskell',               linewidth=1)
    plt.plot(time, bModel, "r",          label='Seismic Model omega^2', linewidth=1)
    plt.plot(time, bModelCube, "orange", label='Seismic Model omega^3', linewidth=1)
    plt.legend(loc = 2, prop={'size': 10})
    plt.xlabel("time (s)")
    plt.ylabel("omega_c (Hz)")
    plt.tight_layout()  

    if displayPlot == False:
        plt.close(plotOmegaC)

    """ return the figure so you can display it if displayPlot == True"""
    return plotOmegaC

# --------------------------------------- plot of amplitude and omega_c calculated in the fit  -------------------------------------- # 
def plotAmp(time, aGL, aHask, aModel ,aModelCube, displayPlot=True):
    plotAmp = plt.figure(1504, figsize=(20,10))
    plt.title("Plot of Amplitude/time")
    plt.plot(time, aGL, "b",             label='GL (corrected)',        linewidth=1)
    plt.plot(time, aHask, "chartreuse",  label='Haskell',               linewidth=1)
    plt.plot(time, aModel, "r",          label='Seismic Model omega^2', linewidth=1)
    plt.plot(time, aModelCube, "orange", label='Seismic Model omega^3', linewidth=1)
    plt.legend(loc = 3, prop={'size': 10})
    plt.yscale('log')
    plt.xlabel("time (s)")
    plt.ylabel("amplitude (m/sqrt(Hz))")
    plt.tight_layout()

    if displayPlot == False:
        plt.close(plotAmp)

    """ return the figure so you can display it if displayPlot == True """
    return plotAmp


# --------------------------------------- plot of amplitude and fit calculated  -------------------------------------- # 
def plotAmpCutFitOmegaC(tCut, ampCut,time, aGL, aHask, aModel, aModelCube, bGL, cHask, bModel, bModelCube, displayPlot=True):
    plotBigFigure = plt.figure(1505, figsize=(20,10))

    plt.subplot(311)
    plt.plot(tCut, ampCut,  color='k', label='amplitude of data', linewidth=0.5) # plot the signal amplitude
    plt.title("Fitting of different models with the data of amplitude of Apollo 15")
    plt.xlabel('time (s)')
    plt.ylabel('amplitude (m/Hz^0.5)')
    plt.legend(loc = 3, prop={'size': 10})                           # legend position
    plt.tight_layout()

    plt.subplot(312)
    plt.title("Plot of omega_coins/time")
    plt.plot(time, bGL, "b",             label='GL (corrected)',        linewidth=1)
    plt.plot(time, cHask, "chartreuse",  label='Haskell',               linewidth=1)
    plt.plot(time, bModel, "r",          label='Seismic Model omega^2', linewidth=1)
    plt.plot(time, bModelCube, "orange", label='Seismic Model omega^3', linewidth=1)
    plt.legend(loc = 1, prop={'size': 10})
    plt.xlabel("time (s)")
    plt.ylabel("omega_c (Hz)")
    plt.tight_layout()

    plt.subplot(313)
    plt.title("Plot of Amplitude/time")
    plt.plot(time, aGL, "b",             label='GL (corrected)',        linewidth=1)
    plt.plot(time, aHask, "chartreuse",  label='Haskell',               linewidth=1)
    plt.plot(time, aModel, "r",          label='Seismic Model omega^2', linewidth=1)
    plt.plot(time, aModelCube, "orange", label='Seismic Model omega^3', linewidth=1)
    # plt.legend(loc = 3, prop={'size': 10})
    plt.yscale('log')
    plt.xlabel("time (s)")
    plt.ylabel("amplitude (m/sqrt(Hz))")
    plt.tight_layout()

    if displayPlot == False:
        plt.close(plotBigFigure)

    """ return the figure so you can display it if displayPlot == True"""
    return plotBigFigure

# --------------------------------------- plot of seismicSpectra.py  -------------------------------------- # 
def plotSeismicSpectra(npas,t,f,gl,gl_f,gl_c_freq,swh1,swh2,swh3,swh_f1,swh_f2,swh_f3, displayPlot=True):
    plotSeismicSpectraFreq = plt.figure(1, figsize=(20,10))
    plt.subplot(1,2,1)
    plt.title("Time Function")
    plt.plot(t, gl,   'k'   ,  linewidth=1, label = "GL tau1 = 0.5")  # plot of gl,  function of t
    plt.plot(t, gl_c_freq, 'b',  linewidth=1, label = "GL corrected")      # plot of gl,  function of t
    plt.plot(t, swh1, 'r--', linewidth=1, label = "SWH B = 0.000")    # plot of swh, function of t
    plt.plot(t, swh2, 'r-.', linewidth=1, label = "SWH B = 0.171")    # plot of swh, function of t
    plt.plot(t, swh3, 'r'  , linewidth=1, label = "SWH B = 0.490")    # plot of swh, function of t
    plt.legend(loc = 1, prop={'size': 10})                            # legend position
    plt.xlim(0,1.5)

    plt.subplot(1,2,2)
    plt.title("Frequency Function")
    plt.plot(f[0:f.size//2], abs(gl_f[0:npas//2]),     'k' ,   linewidth=1)  # plot of FFT, function of f (frequency)
    plt.plot(f[0:f.size//2], abs(gl_c_freq[0:npas//2]),   'b', linewidth=1)  # plot of FFT, function of f (frequency)
    plt.plot(f[0:f.size//2], abs(swh_f1[0:npas//2]), 'r--' , linewidth=1)    # plot of FFT, function of f (frequency)
    plt.plot(f[0:f.size//2], abs(swh_f2[0:npas//2]), 'r-.' , linewidth=1)    # plot of FFT, function of f (frequency)
    plt.plot(f[0:f.size//2], abs(swh_f3[0:npas//2]), 'r'   , linewidth=1)    # plot of FFT, function of f (frequency) up to Nyquist frequency / 2
    plt.ylabel('Normalized Moment derivativ spectrum')
    plt.xscale('log')
    plt.yscale('log')
    plt.xlim(1e-2, 10)
    plt.ylim(1e-2 ,10)
    plt.tight_layout()

    if displayPlot == False:
        plt.close(plotSeismicSpectraFreq)
    
    # return the plot if displayPlot is set to true (default value)
    return plotSeismicSpectraFreq