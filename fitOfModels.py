from scipy.fftpack import fft
import matplotlib.pyplot as plt
import math
import numpy as np
import scipy.optimize as optimization
from scipy.stats import chi2

from fitCalculationScript import CalculFit
from fitPlots import modelFit
from fitPlots import plotOmegaC
from fitPlots import plotAmp
from fitPlots import plotAmpCutFitOmegaC

from seismicSpectra import functionCreation



# ---------------------------- fetching data for fit ------------------------------ # 
numberOfFiles         = 178     # number of files to use
lengthOfData          = 1854    # length of the data in the files (must be the same length)
leftWindow            = 22      # arbitrary left window  boundary for the fit
rightWindow           = 622     # arbitrary right window doundary for the fit (after this boundary, too much noise)
freqDataFull          = np.zeros((numberOfFiles,lengthOfData))
ampFFTDataFull        = np.zeros((numberOfFiles,lengthOfData))

subFileName = "Station16_Event7205130816/S16_LP-SP7205130816-7205131146.dat."
for i in range(1,numberOfFiles):
    if i < 10:
        fileName      = subFileName+"00"+str(i)+".dtr.aligne.zero.PSD.DIS"
    elif i >= 10 and i < 100 :
        fileName      = subFileName+"0"+str(i)+".dtr.aligne.zero.PSD.DIS"
    else :
        fileName      = subFileName+str(i)+".dtr.aligne.zero.PSD.DIS"

    freqDataFull[i]   = np.loadtxt(fileName, usecols=0)
    ampFFTDataFull[i] = np.loadtxt(fileName, usecols=3)


# defines boundaries where we do the fitting
freqDataMin           = 2       # index of minimal frequency to use, start at 2 because inf and nan values before at 0 & 1
freqDataMax           = 1003    # index of maximal frequency to use
sigmaLeft             = 75      # left boundary to use for sigma (Khi square and residual calculation)
sigmaRight            = 152     # right boundary to use for sigma 
freqData              = np.zeros((numberOfFiles,freqDataMax-freqDataMin))
ampFFTData            = np.zeros((numberOfFiles,freqDataMax-freqDataMin))


for i in range(len(freqDataFull)):
    freqData[i]       = freqDataFull[i][freqDataMin:freqDataMax]
    ampFFTData[i]     = ampFFTDataFull[i][freqDataMin:freqDataMax]

# ------------------- fitting of the models on all of the frames -------------------- #
aGL                   = np.zeros(len(freqData))
bGL                   = np.zeros(len(freqData))
aHask                 = np.zeros(len(freqData))
bHask                 = np.zeros(len(freqData))
cHask                 = np.zeros(len(freqData))
aModel                = np.zeros(len(freqData))
bModel                = np.zeros(len(freqData))
aModelCube            = np.zeros(len(freqData))
bModelCube            = np.zeros(len(freqData))
yGL                   = np.zeros((len(freqData), freqDataMax-freqDataMin))
yHask                 = np.zeros((len(freqData), freqDataMax-freqDataMin))
yModel                = np.zeros((len(freqData), freqDataMax-freqDataMin))
yModelCube            = np.zeros((len(freqData), freqDataMax-freqDataMin))
poptGL                = np.zeros((len(freqData), 2))
poptHaskell           = np.zeros((len(freqData), 3))
poptModel             = np.zeros((len(freqData), 2))
poptModelCube         = np.zeros((len(freqData), 2))
residuGL              = np.zeros(len(freqData))
residuHask            = np.zeros(len(freqData))
residuModel           = np.zeros(len(freqData))
residuModelCube       = np.zeros(len(freqData))
meanAmpFFT            = np.zeros(len(freqData))
sigma                 = np.zeros(len(freqData))

guessGLa              = 0    # initial guess of value of omega_c to pass to take less time to process
guessGLb              = 0    # initial guess of value of A 
guessHaska            = 0
guessHaskb            = 0
guessHaskc            = 0
guessModela           = 0
guessModelb           = 0
guessModelCubea       = 0
guessModelCubeb       = 0

saveResults           = False   # for saving results of fit, set it to True
loadResults           = False   # for loading results of fit that were previously calculated, set it to True


for i in range(len(freqData)):
    print("----------------",i,"----------------")
    fit = CalculFit(0.005, freqData[i], ampFFTData[i], freqDataMin, freqDataMax, guessGLa, guessHaska, guessModela, guessModelCubea, guessGLb, guessHaskb, guessModelb, guessModelCubeb, guessHaskc, sigmaLeft, sigmaRight, leftWindow, rightWindow)

    # GL: y = (A*f*2*math.pi) / (omega_c*(1+(f/omega_c)**3))
    aGL[i]             = fit.aGL         # a = A
    bGL[i]             = fit.bGL         # b = omega_C
    yGL[i]             = fit.yGL

    # Haskell: y = alpha * ( (1+(A**2)*(omega/k)**2) * (1+ (omega/k)**2)**-5 )
    aHask[i]           = fit.aHask       # a = alpha
    bHask[i]           = fit.bHask       # b = A
    cHask[i]           = fit.cHask       # c = k (omega_c)
    yHask[i]           = fit.yHask

    # 1st seismic model: omega^2 : y = A / (1 + (f/omega_c)**2)
    aModel[i]          = fit.aModel      # a = A
    bModel[i]          = fit.bModel      # b = omega_c
    yModel[i]          = fit.yModel      # b = omega_c

    # 2nd seismic model: omega cube : y =  A / ( (1 + (f/omega_c)**2) )**(3.0/2)
    aModelCube[i]      = fit.aModelCube  # a = A
    bModelCube[i]      = fit.bModelCube  # b = omega_c
    yModelCube[i]      = fit.yModelCube  # b = omega_c

    # fetching the other data from the class CalculFit
    poptGL[i]          = fit.popt
    poptHaskell[i]     = fit.poptHaskell
    poptModel[i]       = fit.poptModel
    poptModelCube[i]   = fit.poptModelCube

    residuGL[i]        = fit.residuGL
    residuHask[i]      = fit.residuHask
    residuModel[i]     = fit.residuModel
    residuModelCube[i] = fit.residuModelCube
    
    meanAmpFFT[i]      = fit.meanAmpFFT
    sigma[i]           = fit.sigma    

    guessGLa           = fit.popt[0]
    guessHaska         = fit.poptHaskell[0]
    guessModela        = fit.poptModel[0]
    guessModelCubea    = fit.poptModelCube[0]

    guessGLb           = fit.popt[1]
    guessHaskb         = fit.poptHaskell[1]
    guessModelb        = fit.poptModel[1]
    guessModelCubeb    = fit.poptModelCube[1]

    guessHaskc         = fit.poptHaskell[2]



# calcul of minimum and maximum ampFFTData for plotting the fit:
maxAmpFFTData = ampFFTData.max()
minAmpFFTData = 1e10
for i in range(len(ampFFTData)):
    for j in range(len(ampFFTData[i])):
        if minAmpFFTData > ampFFTData[i][j] and ampFFTData[i][j] > 2e-19:
            minAmpFFTData = ampFFTData[i][j]


# -------------------------------- save the results to avoid doing it again ------------------------------- #
if saveResults:
    np.savetxt('../data/fit/AGL.txt',                    aGL)        # A
    np.savetxt('../data/fit/BGL.txt',                    bGL)        # omega_c

    np.savetxt('../data/fit/alphaHaskell.txt',           aHask)      # alpha (the real amplitude)
    np.savetxt('../data/fit/AHaskell.txt',               bHask)      # A
    np.savetxt('../data/fit/BHaskell.txt',               cHask)      # k (omega_c)

    np.savetxt('../data/fit/ASeismicModel.txt',          aModel)     # A
    np.savetxt('../data/fit/BSeismicModel.txt',          bModel)     # omega_c

    np.savetxt('../data/fit/ASeismicModelCube.txt',      aModelCube) # A
    np.savetxt('../data/fit/BSeismicModelCube.txt',      bModelCube) # omega_c

    np.savetxt('../data/fit/residuGL.txt',               residuGL)
    np.savetxt('../data/fit/residuHaskell.txt',          residuHask)
    np.savetxt('../data/fit/residuSeismicModel.txt',     residuModel)
    np.savetxt('../data/fit/residuSeismicModelCube.txt', residuModelCube)

# Loading instead of calculating, If the calcul and saving has been done before avoid to calcul again (time cost effective)
if loadResults:
    aGL = np.loadtxt('../data/fit/AGL.txt')                      # A
    bGL = np.loadtxt('../data/fit/BGL.txt')                      # omega_c

    aHask = np.loadtxt('../data/fit/alphaHaskell.txt')           # alpha (the real A)
    bHask = np.loadtxt('../data/fit/AHaskell.txt')               # A
    cHask = np.loadtxt('../data/fit/BHaskell.txt')               # k (omega_c)

    aModel = np.loadtxt('../data/fit/ASeismicModel.txt')         # A
    bModel = np.loadtxt('../data/fit/BSeismicModel.txt')         # omega_c

    aModelCube = np.loadtxt('../data/fit/ASeismicModelCube.txt') # A
    bModelCube = np.loadtxt('../data/fit/BSeismicModelCube.txt') # omega_c

    residuGL = np.loadtxt('../data/fit/residuGL.txt')
    residuHask = np.loadtxt('../data/fit/residuHaskell.txt')
    residuModel = np.loadtxt('../data/fit/residuSeismicModel.txt')
    residuModelCube = np.loadtxt('../data/fit/residuSeismicModelCube.txt')

#################################################################################################################################
##################################################### Calcul of Chi Square  #####################################################
#################################################################################################################################
frame              = 23                             # frame of the best fist in which watch the plot and do the Chi 2 test
ddofHask           = rightWindow - leftWindow - 3   # degree of freedom of Haskell 
ddofOther          = rightWindow - leftWindow - 2   # degree of freedom of the other models
steps              = np.arange((freqDataMax-freqDataMin))
df                 = np.arange(0,21)
meanFrame          = ampFFTData[frame].mean()

GLChiSquare        = np.sum(((np.log10(yGL[frame])        - np.log10(ampFFTData[frame]))**2) / sigma[frame])
HaskChiSquare      = np.sum(((np.log10(yHask[frame])      - np.log10(ampFFTData[frame]))**2) / sigma[frame])
ModelChiSquare     = np.sum(((np.log10(yModel[frame])     - np.log10(ampFFTData[frame]))**2) / sigma[frame])
ModelCubeChiSquare = np.sum(((np.log10(yModelCube[frame]) - np.log10(ampFFTData[frame]))**2) / sigma[frame])

GLChiSquare        = 1 - chi2.cdf(GLChiSquare, ddofOther)
HaskChiSquare      = 1 - chi2.cdf(HaskChiSquare, ddofOther)
ModelChiSquare     = 1 - chi2.cdf(ModelChiSquare, ddofOther)
ModelCubeChiSquare = 1 - chi2.cdf(ModelCubeChiSquare, ddofOther)

print("------ p-value of model's Khi Square for frame number",frame,"------")
print("GLChiSquare        = ", GLChiSquare)
print("HaskChiSquare      = ", HaskChiSquare)
print("ModelChiSquare     = ", ModelChiSquare)
print("ModelCubeChiSquare = ", ModelCubeChiSquare)
print("omega C Haskell    = ", poptHaskell[frame][2])
print("--------------------------------------------------------------------")


#########################################################################################################################################
################################################### preparing data for plot of models ###################################################
#########################################################################################################################################
# data for plotting over time
timeStart    = 3490                                         # known value
timeStop     = 8045                                         # known value
timeWindow   = timeStop - timeStart
timeDivision = timeWindow / numberOfFiles                   # numberOfFiles = 178 here
time         = np.arange(timeStart,timeStop,timeDivision)

fileAdress   = '../data/S16_LP-SP7205130816-7205131146.dat' # the file adress path to use
tFull        = np.loadtxt(fileAdress, usecols = 0)          # time
ampFull      = np.loadtxt(fileAdress, usecols = 1)          # amplitude (NOT the FFT)
leftBorder   = 0                                            # left border of time to display the plot
rightBorder  = 0

for i in range(len(tFull)):                                 # calculate left border (set it to 0 if you want to take the signal at the start)
    if tFull[i] > timeStart:
        leftBorder = i
        break

for i in range(len(tFull)):                                 # calculate right border (set it to lent(tFull) if you want to take the signal at the start)
    if tFull[i] > timeStop:
        rightBorder = i
        break

tCut   = tFull[leftBorder:rightBorder]
ampCut = ampFull[leftBorder:rightBorder]


#############################################################################################################################################
################################################################### Plot ####################################################################
#############################################################################################################################################

# -------------------------------------------- plot of the paper Daubar, Lognonne et al. functions ------------------------------------------ # 
# creation of the plot of the paper Daubar, Lognonne et al.
functionCreation()

# ------------------------------ plot of the fit, of the cutoff frequency (omega_c), amplitude and amplitude cut  ------------------------------ # 
plotPathName   = "../../plots_saved/fitting/fit_stations/Station16_Event7205130816/"
modelFit(frame, numberOfFiles, freqData, ampFFTData, meanAmpFFT, minAmpFFTData, maxAmpFFTData, leftWindow, rightWindow, yGL, yHask, yModel, yModelCube, plotPathName, DoLoop=True, exportThisFit=False, displayPlot=False)
plotOmegaC(time, bGL, cHask, bModel, bModelCube, displayPlot=True)
# plotAmp(time, aGL, aHask, aModel ,aModelCube, displayPlot=True)
# plotAmpCutFitOmegaC(tCut, ampCut,time, aGL, aHask, aModel, aModelCube, bGL, cHask, bModel, bModelCube, displayPlot=True)


plt.show()
plt.close()