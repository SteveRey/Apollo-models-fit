import math
import numpy as np
import scipy.optimize as optimization

class CalculFit:
    """ Class calculating for every frame the fit of the 4 models:
    GL (= Gudkova), Haskell, Seismic Model omega^2, Seismic Model omega^3

    Mandatory arguments:
        - stepsForFit: size of steps you want to make to try to calcul the fit. The smaller, the better the fit and the higher the time it takes.                 
        - freqDataArray: 1D N array containing frequency data (must be the same dimension as amplitudeDataArray)
        - amplitudeDataArray: 1D N array containing amplitude data in frequency (e.g. : created with an FFT)
        - frequencyDataMin, Max:  left and right borders of the window frequencies to take 
        - guess of a, b (and c for Haskell): initial guess for the 3rd value of the fit of Haskell (only this one cause problems). 
            --> Best way to do so: retrieve the value of poptHaskell[2] and pass it in the function CalculFit, as the last parameter when you use it.
                    EXAMPLE OF USE : 
                    for i in range len(freqArray):
                        guess = 1      # if you don't know which value to set (default value of optimization.curve_fit)
                        name_of_fit.CalculFit(freqDArray[i], amplitudeFreqArray[i], MinFrequency, MaxFrequency, guess)
                        guess = name_of_fit.poptHaskell[2]
        - sigma left and right boundaries for calculation residuals of the models, and Khi square values

   Optionnal arguments:
        left and right Window: boundaries for the fitting. Default to use all the data (from frequencyDataMin to frequencyDataMax).
            --> You  may want to fit on a restricted window of your data if your data contains too much noise after a certain frequency.
            --> Make sure you don't have inf or nan value in your arrays of it won't work.
            

    Return:
        - a, b (and c for Haskell) values for the functions and the functions y taking thoses parameters:
            GL (Gudkova)          : aGL,        bGL,                 yGL
            Haskell               : aHask,      bHack,      cHask,   yHask
            Seismic Model omega^2 : aModel,     bModel,              yModel
            Seismic Model omega^3 : aModelCube, bModelCube,          yModelCube
                --> example in your main program : aGudkova = name_of_fit.aGL
        
        - left and right window for residual calculation, number of steps between them, and mean amplitude between them:  
        - n                       : number of steps between them
        - meanAmpFFT              : mean of amplitude between left and right window
        - meanAmpFFTLog           : log of meanAmpFFT 
            --> example in your main program: mean_of_amplitude_between_borders = name_of_fit.meanAmpFFT
        - the log of: yGL, yHask, and y of Seismic models:
            yGL_Log
            yHask_Log
            yModel_Log
            yModelCube_Log

        the residuals of thoses models :
        - sigma                   : for the calcul of the residuals of the models
        - residuGL                : residual of Gudkova
        - residuHask              : residual of Haskell
        - residuModel             : residual of omega^2 seismic model
        - residuModelCube         : residual of omega^3 seismic model
            --> example in your main program    : 
                    --> with 1 frame            : residual_of_Haskell    = name_of_fit.residuHask
                    --> with a loop             : for i in range len(freqArray):
                                                    name_of_fit.CalculFit(freqDArray[i], amplitudeFreqArray[i], MinFrequency, MaxFrequency, guess)
                                                    residual_of_Haskell[i] = name_of_fit.residuHask
    """
    
    # -----------------------  function gl_c_f (GL corrected) for GL fitting ---------------------- #
    # # inconnues : A (amplitude), et omega_c (= corner frequency))
    def gl_c_f(self,f,A,omega_c):
        return  (A*f*2*math.pi) / (omega_c*(1+(f/omega_c)**3) )

    # ----------------------------- function Haskell for SWH fitting ------------------------------ #
    # # inconnues : alpha (amplitude), A, et k (= frequency s^-1)) 
    def haskell(self,f,alpha,A,k):
        return alpha * ( (1+(A**2)*(f/k)**2) * (1+(f/k)**2)**-5 )

    # ---------------------- function of the wave spectrum of seismic model ----------------------- #
    # # inconnues : alpha (amplitude ici), A, et omega 
    def modelSeism(self,f,A,omega_c):
        return A / (1 + (f/omega_c)**2)

    # ---------------------- function of the wave spectrum of seismic model ----------------------- #
    # # inconnues : alpha (amplitude ici), A, et omega 
    def modelSeismCube(self,f,A,omega_c):
        return A / ( (1 + (f/omega_c)**2) )**(3.0/2)

    def __init__(self, stepsForFit, freqDataArray, amplitudeDataArray, frequencyDataMin, frequencyDataMax, guessGLa, guessHaska, guessModela, guessModelCubea, guessGLb, guessHaskb, guessModelb, guessModelCubeb, guessHaskc, sigmaLeft, sigmaRight, leftWindow=0, rightWindow=-1):
        if freqDataArray.size == amplitudeDataArray.size and freqDataArray.ndim == 1 and amplitudeDataArray.ndim == 1: 
            """ fetching the variables """
            self.stepsForFit        = stepsForFit
            self.freqDataArray      = freqDataArray
            self.amplitudeDataArray = amplitudeDataArray
            self.frequencyDataMin   = frequencyDataMin
            self.frequencyDataMax   = frequencyDataMax

            """ finding the best fit by calculating the residual error """
            self.leftWindow         = leftWindow                   # left border for calculating the residual
            self.rightWindow        = rightWindow                   # right border for calculating the residual
            self.n                  = self.rightWindow - self.leftWindow          # number of points between left and right border
  

            """ fit of GL model """
            # loop for solving problem on Gudkova
            continueSearching = True
            try:                 
                # 1st try: try to fit with provided values
                self.popt, self.pcov =  optimization.curve_fit(self.gl_c_f, self.freqDataArray[self.leftWindow:self.rightWindow], \
                    self.amplitudeDataArray[self.leftWindow:self.rightWindow], bounds=(-np.inf,np.inf))  
            except:
                self.guessGLa = guessGLa * 1
                self.guessGLb = guessGLb * 1
                while self.guessGLb > guessGLb / 20:
                    try:
                        self.popt, self.pcov =  optimization.curve_fit(self.gl_c_f, self.freqDataArray[self.leftWindow:self.rightWindow], \
                            self.amplitudeDataArray[self.leftWindow:self.rightWindow], p0=[self.guessGLa, self.guessGLb], bounds=(-np.inf,np.inf))  
                        continueSearching = False
                        break
                    except:
                        self.guessGLb -= stepsForFit
                
                if continueSearching:
                    self.guessGLa = 1
                    self.guessGLb = 1  
                    while True:
                        try:
                            self.popt, self.pcov =  optimization.curve_fit(self.gl_c_f, self.freqDataArray[self.leftWindow:self.rightWindow], \
                                    self.amplitudeDataArray[self.leftWindow:self.rightWindow], p0=[self.guessGLa, self.guessGLb], bounds=(-np.inf,np.inf))  
                            break
                        except:
                            self.guessGLb -= stepsForFit

            
            """       fit of Haskell       """
            """  note: Haskell fit may encounter some problem caused by the 3rd variable """            
            continueSearching = True
            try:
                self.poptHaskell, self.pcovHaskell =  optimization.curve_fit(self.haskell,self.freqDataArray[self.leftWindow:self.rightWindow], \
                self.amplitudeDataArray[self.leftWindow:self.rightWindow], bounds=(-np.inf,np.inf))
            except: 
                self.guessHaska = guessHaska * 1
                self.guessHaskb = guessHaskb * 1
                self.guessHaskc = guessHaskc * 1
                while self.guessHaskc > guessHaskc / 20:
                    try:
                        self.poptHaskell, self.pcovHaskell =  optimization.curve_fit(self.haskell,self.freqDataArray[self.leftWindow:self.rightWindow], \
                            self.amplitudeDataArray[self.leftWindow:self.rightWindow], p0=[self.guessHaska, self.guessHaskb, self.guessHaskc], bounds=(-np.inf,np.inf))   
                        continueSearching = False
                        break
                    except:
                        self.guessHaskc -= stepsForFit
                if continueSearching:                
                    self.guessHaska = 1
                    self.guessHaskb = 1
                    self.guessHaskc = 1
                    # loop for solving problem on Haskell 
                    while True: 
                        try:
                            self.poptHaskell, self.pcovHaskell =  optimization.curve_fit(self.haskell,self.freqDataArray[self.leftWindow:self.rightWindow], \
                                self.amplitudeDataArray[self.leftWindow:self.rightWindow], p0=[self.guessHaska, self.guessHaskb, self.guessHaskc], bounds=(-np.inf,np.inf)) 
                            break
                        except:
                            self.guessHaskc -= stepsForFit
            for i in range(3):
                if self.poptHaskell[i] == 0:
                    self.poptHaskell[i] = 1e-19                   
           
            """ fit of 1st seismic model (omega square) """   
            continueSearching = True         
            try:
                self.poptModel, self.pcovModel =  optimization.curve_fit(self.modelSeism, self.freqDataArray[self.leftWindow:self.rightWindow], \
                    self.amplitudeDataArray[self.leftWindow:self.rightWindow], bounds=(-np.inf,np.inf))
            except:
                self.guessModela = guessModela * 1
                self.guessModelb = guessModelb * 1
                while self.guessModelb > guessModelb  / 20:
                    try:
                        self.poptModel, self.pcovModel =  optimization.curve_fit(self.modelSeism, self.freqDataArray[self.leftWindow:self.rightWindow], \
                            self.amplitudeDataArray[self.leftWindow:self.rightWindow],  p0=[self.guessModela, self.guessModelb], bounds=(-np.inf,np.inf)) 
                        continueSearching = False
                        break
                    except:
                        self.guessModelb -= stepsForFit  

                if continueSearching:
                    self.guessModela = 1
                    self.guessModelb = 1
                    while True:
                        try:
                            self.poptModel, self.pcovModel =  optimization.curve_fit(self.modelSeism, self.freqDataArray[self.leftWindow:self.rightWindow], \
                                self.amplitudeDataArray[self.leftWindow:self.rightWindow],  p0=[self.guessModela, self.guessModelb], bounds=(-np.inf,np.inf))
                            break
                        except:
                            self.guessModelb -= stepsForFit


            """ fit of 2st seismic model (omega cube) """   
            continueSearching = True         
            try:
                self.poptModelCube, self.pcovModelCube =  optimization.curve_fit(self.modelSeismCube, self.freqDataArray[self.leftWindow:self.rightWindow], \
                    self.amplitudeDataArray[self.leftWindow:self.rightWindow], bounds=(-np.inf,np.inf))
            except:
                self.guessModelCubea = guessModelCubea * 1
                self.guessModelCubeb = guessModelCubeb * 1
                while self.guessModelCubeb > guessModelCubeb / 20:
                    try:
                        self.poptModelCube, self.pcovModelCube =  optimization.curve_fit(self.modelSeismCube, self.freqDataArray[self.leftWindow:self.rightWindow], \
                            self.amplitudeDataArray[self.leftWindow:self.rightWindow], p0=[self.guessModelCubea, self.guessModelCubeb], bounds=(-np.inf,np.inf))
                        continueSearching = False
                        break
                    except:
                        self.guessModelCubeb -= stepsForFit

                if continueSearching:
                    self.guessModelCubea = 1
                    self.guessModelCubeb = 1
                    while True:
                        try:
                            self.poptModelCube, self.pcovModelCube =  optimization.curve_fit(self.modelSeismCube, self.freqDataArray[self.leftWindow:self.rightWindow], \
                                self.amplitudeDataArray[self.leftWindow:self.rightWindow], p0=[self.guessModelCubea, self.guessModelCubeb], bounds=(-np.inf,np.inf))
                            break
                        except:
                            self.guessModelCubeb -= stepsForFit
            
            
            """ set the variables a, b (and c for Haskell), and their respective function """
            """ taking the absolute value because sometime the frequency is negative  """
            # for GL
            self.aGL = abs(self.popt[0])
            self.bGL = abs(self.popt[1])
            self.yGL = (self.aGL*self.freqDataArray*2*math.pi) / (self.bGL*(1+(self.freqDataArray/self.bGL)**3)) 

            # for Haskell
            self.aHask = abs(self.poptHaskell[0])
            self.bHask = abs(self.poptHaskell[1])
            self.cHask = abs(self.poptHaskell[2])
            self.yHask = self.aHask*((1+(self.bHask**2)*(self.freqDataArray/self.cHask)**2) * (1+(self.freqDataArray/self.cHask)**2)**-5)

            # for 1st seismic model
            self.aModel = abs(self.poptModel[0])
            self.bModel = abs(self.poptModel[1])
            self.yModel = self.aModel / (1+(self.freqDataArray/self.bModel)**2)

            # for 2nd seismic model (modelCube)
            self.aModelCube = abs(self.poptModelCube[0])
            self.bModelCube = abs(self.poptModelCube[1])
            self.yModelCube = self.aModelCube / ( (1+(self.freqDataArray/self.bModelCube)**2)**(3.0/2))

            """ calculate the mean amplitude in the window provided """
            self.sigmaLeft  = sigmaLeft     # pass thoses arguments to function
            self.sigmaRight = sigmaRight 
            self.meanAmpFFT = self.amplitudeDataArray[sigmaLeft:sigmaRight].mean()

            """ taking the log of the data """
            if self.meanAmpFFT < 10e-31:
                 self.meanAmpFFT = 1e-19
            self.meanAmpFFTLog  = np.log10(self.meanAmpFFT)
            # remove the 0 for the log10
            for i in range(len(amplitudeDataArray)):
                if self.amplitudeDataArray[i]  <= 1e-19:
                    self.amplitudeDataArray[i]  = 1e-19
            self.ampFFTDataLog  = np.log10(self.amplitudeDataArray)
            # eliminate negative, nan, inf or 0 values for GL
            for i in range(len(self.yGL)):
                if self.yGL[i] <= 0 or math.isnan(self.yGL[i]) or math.isinf(self.yGL[i]):
                    self.yGL[i] = 1e-19
                if self.yHask[i] <= 0 or math.isnan(self.yHask[i]) or math.isinf(self.yHask[i]):
                    self.yHask[i] = 1e-19
                if self.yModel[i] <= 0 or math.isnan(self.yModel[i]) or math.isinf(self.yModel[i]):
                    self.yModel[i] = 1e-19
                if self.yModelCube[i] <= 0 or math.isnan(self.yModelCube[i]) or math.isinf(self.yModelCube[i]):
                    self.yModelCube[i] = 1e-19
            # calcul the log10 of the values
            self.yGL_Log        = np.log10(self.yGL)
            self.yHask_Log      = np.log10(self.yHask)
            self.yModel_Log     = np.log10(self.yModel)
            self.yModelCube_Log = np.log10(self.yModelCube)

            """ calculate the sigma of the data """
            self.sigma           = np.sum( np.sqrt( ((self.ampFFTDataLog[sigmaLeft:sigmaRight] - self.meanAmpFFTLog)**2)/ self.n )) 
            # to avoid sigma = 0:
            if self.sigma < 10e-31:
                self.sigma = 1e-19
            """ calculate the residual compared to the data """
            self.residuGL        = np.sum( ((self.ampFFTDataLog[self.leftWindow:self.rightWindow] - self.yGL_Log[self.leftWindow:self.rightWindow])        / self.sigma)**2)
            self.residuHask      = np.sum( ((self.ampFFTDataLog[self.leftWindow:self.rightWindow] - self.yHask_Log[self.leftWindow:self.rightWindow])      / self.sigma)**2)
            self.residuModel     = np.sum( ((self.ampFFTDataLog[self.leftWindow:self.rightWindow] - self.yModel_Log[self.leftWindow:self.rightWindow])     / self.sigma)**2)
            self.residuModelCube = np.sum( ((self.ampFFTDataLog[self.leftWindow:self.rightWindow] - self.yModelCube_Log[self.leftWindow:self.rightWindow]) / self.sigma)**2)

        else:
            raise TypeError("ERROR : arrays must be both 1D and have and the same size.\n\
            If you have splitted your data, try to do a loop instead in your main programm.\n\
            Program Stopped.")