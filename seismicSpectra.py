import math
import numpy as np

from fitPlots import plotSeismicSpectra

def functionCreation() :

    #############################################################################################################
    ############################## Creating the functions GL, GL corected and SWH  ##############################
    #############################################################################################################
    npas      = 32768                                # steps for discretization
    t         = np.linspace(0,200,npas)              # time discretized (array of npas elements, from 0 to 200)     
    f         = np.fft.fftfreq(t.size, d=1.0/200)    # frequency (for Fourier Transform)  d = total sample spacing

    # ----------------------------- GL ------------------------------ # 
    tau1    = 0.5                            # in secondes; = pi/omega1
    omega1  = math.pi/tau1                   # = 2*pi
    gl      = np.zeros(npas)                 # derivative of GL function (time function)
    # correction of gl:
    gl_c_freq  = np.zeros(npas)               # GL corrected to avoid variation
    omega_c = omega1/3.61                     # omega corner (cutoff frequency) --> decrease  =  stronger variation of gl_c_freq
    A       = 0.67                            # amplitude/magnitude

    # derivative of GL:
    for i in range(npas):
        if  t[i] > (-tau1) and t[i] < (tau1):
            gl[i]     = 2.125*omega1*math.sin(2*omega1*t[i])   # 2.15: amplitude for normalization

        else:
            gl[i]     = 0


    gl_c_freq = (A*t*2*math.pi) / (omega_c*(1+(t/omega_c)**3) )

    # ----------------------------- SWH ------------------------------ # 
    swh1   = np.zeros(npas)                 # initialization of the array swh (the function we use)
    swh2   = np.zeros(npas)                 # initialization of the array swh (the function we use)
    swh3   = np.zeros(npas)                 # initialization of the array swh (the function we use)
    tau0   = 1.0/20
    B      = [0.240, 0.171, 0.490]          # normalement : B  = [0, 0.171, 0.490] pour plot swh papier Lognonne
    tabK   = [31.6, 28.4, 17.0]
    # calcul of the derivative:
    swh1   = (1/tau0)*np.exp((0-t)/tau0)*(1 + t/tau0 + (t**2)/(2*tau0**2) + (t**3)/(3*tau0**3) - B[0]*t**4/tau0**4) - np.exp((0-t)/tau0)*(1/tau0 + t/tau0**2 + t**2/tau0**3 - B[0]*(4*t**3)/tau0**4)
    swh2   = (1/tau0)*np.exp((0-t)/tau0)*(1 + t/tau0 + (t**2)/(2*tau0**2) + (t**3)/(3*tau0**3) - B[1]*t**4/tau0**4) - np.exp((0-t)/tau0)*(1/tau0 + t/tau0**2 + t**2/tau0**3 - B[1]*(4*t**3)/tau0**4)
    swh3   = (1/tau0)*np.exp((0-t)/tau0)*(1 + t/tau0 + (t**2)/(2*tau0**2) + (t**3)/(3*tau0**3) - B[2]*t**4/tau0**4) - np.exp((0-t)/tau0)*(1/tau0 + t/tau0**2 + t**2/tau0**3 - B[2]*(4*t**3)/tau0**4)

    ##### Fourier transform #####
    gl_f   = np.fft.fft(gl,   norm="ortho") # Fourier transform of gl, ortho ---> gl_f = gl_f  / np.sum(gl)
    swh_f1 = np.fft.fft(swh1, norm="ortho") # Fourier transform of swh1 
    swh_f2 = np.fft.fft(swh2, norm="ortho") # Fourier transform of swh2
    swh_f3 = np.fft.fft(swh3, norm="ortho") # Fourier transform of swh3 

    #############################################################################################################
    ################################### fitting the functions with data #########################################
    #############################################################################################################
    # ------------------------ saving in case of use in another programm -------------------------- # 
    np.savetxt('../data/t.txt',      t) # time
    np.savetxt('../data/f.txt',      f) # frequency
    ##### GL #####
    np.savetxt('../data/gl.txt',     gl)
    np.savetxt('../data/gl_f.txt',   abs(gl_f))
    ##### SWH #####
    np.savetxt('../data/swh1.txt',   swh1)
    np.savetxt('../data/swh2.txt',   swh2)
    np.savetxt('../data/swh3.txt',   swh3)
    np.savetxt('../data/swh_f1.txt', swh_f1)
    np.savetxt('../data/swh_f2.txt', swh_f2)
    np.savetxt('../data/swh_f3.txt', swh_f3)

    plotSeismicSpectra(npas,t,f,gl,gl_f,gl_c_freq,swh1,swh2,swh3,swh_f1,swh_f2,swh_f3, displayPlot=True)